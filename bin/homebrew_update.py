#!/usr/bin/env python

import httplib
import datetime
import subprocess
import sys
import time

def connection_active():
    conn = httplib.HTTPConnection('www.google.com')
    try:
        conn.request('HEAD', '/')
        return True
    except:
        return False

def main():
    time.sleep(60)
    dt = datetime.datetime.now()
    print('[ {0} ]: Started update'.format(dt.strftime('%Y-%m-%d %I:%M %p')))

    if connection_active():
        p = subprocess.Popen(['/usr/local/bin/brew', 'update'],
                                stdout=subprocess.PIPE)
        q = subprocess.Popen(['/usr/local/bin/brew', 'upgrade'],
                                stdout=subprocess.PIPE)

        update_output= p.communicate()[0].rstrip('\n')
        upgrade_output = q.communicate()[0].rstrip('\n')

        print(update_output)
        print(upgrade_output)

    else:
        print('No network connectivity')

if __name__ == '__main__':
    main()
