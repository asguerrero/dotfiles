#!/bin/bash

# Update brew
echo "Update started at $(date)"

brew update
brew update > ~/log/update_actual
brew upgrade --all
brew upgrade --all > ~/log/upgrade_actual

# Update npm
#npm update -g npm > /dev/null;

# Update pip
#pip install -U pip > /dev/null;
#pip3 install -U pip > /dev/null;

# print to log
echo "Update complete at $(date)"
