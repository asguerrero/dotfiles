#!/bin/bash

# make symlinks
#$HOME/dotfiles/link_dotfiles.sh
#$HOME/dotfiles/link_bin.sh
#$HOME/dotfiles/link_agents.sh

# Create directories
dirs=( bin Development log .vim/autoload .vim/backup .vim/swap .vim/undo )

#for d in "${dirs[@]}"
#do
#  mkdir $HOME/"${d}"
#done

# Setup ssh key
#ssh-keygen -t rsa -b 4096 -C "asguerrero3@gmail.com"
#eval "$(ssh-agent -s)"
#ssh-add $HOME/.ssh/id_rsa

# install brew and update
$HOME/dotfiles/brew.sh

# install atom packages
$HOME/dotfiles/atom.sh

# Install antigen
git clone https://github.com/zsh-users/antigen.git $HOME/.antigen

# Install vim-plug
curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

sudo softwareupdate -i -a

$HOME/dotfiles/osx_defaults.sh

# Install gems
#gem install tmuxinator
