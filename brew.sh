#!/bin/bash

# Homebrew binaries
BINARIES=(
  homebrew/dupes/awk
  bash
  bash-completion
  binutils
  colordiff
  coreutils
  cmake
  cppcheck
  findutils
  gcc
  git
  homebrew/dupes/grep
  homebrew/dupes/gdb
  netcat
  #node
  nmap
  'homebrew/dupes/openssh --with-brewed-openssl --with-keychain-support'
  pkg-config
  python
  python3
  #boost
  #boost-python
  #postgresql
  tmux
  rbenv
  rbenv-bundler
  rbenv-gem-rehash
  ruby-build
  reattach-to-use-namespace
  ssh-copy-id
  wget
  'vim --with-python --with-ruby --with-perl --with-lua --override-system-vim'
  zsh
)

# Cask applications
APPS=(
  alfred
  android-file-transfer
  appcleaner
  applepi-baker
  arduino
  atom
  audacity
  caffeine
  dash
  dropbox
  eagle
  gcc-arm-embedded
  google-chrome
  google-drive
  iterm2
  mactex
  osxfuse
  #screenhero
  paintbrush
  seil
  serial
  skim
  vlc
  vmware-fusion
  xquartz
)

# Check for homebrew
# Install it if it isn't installed
if test ! $(which brew); then
  echo "Installing homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  brew tap homebrew/dupes 2>/dev/null
  brew tap homebrew/science 2>/dev/null
  brew tap homebrew/python 2>/dev/null
  brew tap homebrew/versions 2>/dev/null
fi

# Tap stuff
brew tap homebrew/dupes 2>/dev/null
brew tap homebrew/science 2>/dev/null
brew tap homebrew/python 2>/dev/null
brew tap homebrew/versions 2>/dev/null

# Update brew
echo "Updating brew"
brew update
brew upgrade

echo "Installing brew binaries"
for binary in "${BINARIES[@]}"; do
  brew install $binary 2>/dev/null
done

#Installing brew-cask if it doesn't exist
if test ! $(which brew-cask); then
  echo "Installing brew cask"
  brew install caskroom/cask/brew-cask
  brew tap caskroom/versions 2>/dev/null
  brew tap caskroom/fonts 2>/dev/null
fi

echo "Installing apps"
brew cask install --appdir="/Applications" ${APPS[@]} 2>/dev/null
