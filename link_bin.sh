#!/bin/bash
############################
# .make.sh
# This script creates symlinks from the home directory to any desired dotfiles in ~/bin
############################

########## Variables

srcdir=$(pwd)/bin                    # dotfiles directory
dstdir=$HOME/bin                    # dotfiles directory

##########

# create dotfiles_old in homedir
echo "Linking to ~/bin"

# change to the bindir directory
#cd ${srcdir}

files=$(ls ${srcdir})

cd ${srcdir}

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
    ln -s ${srcdir}/${file} ${dstdir}/${file}

done
