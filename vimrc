" vim-plug
call plug#begin('~/.vim/plugged')

" Plugins
Plug 'chrishunt/color-schemes'
Plug 'altercation/vim-colors-solarized'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/rainbow_parentheses.vim'

" Code stuff
Plug 'scrooloose/syntastic'

" NERD
Plug 'scrooloose/nerdCommenter'

"Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'Lokaltog/vim-easymotion'
Plug 'bling/vim-airline'
Plug 'Yggdroot/indentLine'
Plug 'bronson/vim-trailing-whitespace'
Plug 'majutsushi/tagbar'
"Plug 'ervandew/supertab'

" autocomplete
"Plug 'Shougo/neocomplete'
"Plug 'Shougo/neosnippet'
"Plug 'Valloric/YouCompleteMe', { 'do': './install.sh --clang-completer' }
"Plug 'MarcWeber/vim-addon-mw-utils'
"Plug 'garbas/vim-snipmate'
"Plug 'honza/vim-snippets'

" tmux
Plug 'christoomey/vim-tmux-navigator'
Plug 'edkolev/tmuxline.vim'

" Powerline
"Plug 'powerline/powerline'

" Web
Plug 'mattn/emmet-vim'

call plug#end()

filetype plugin on
filetype indent on

runtime! plugin/sensible.vim
set autoread
set mouse=a
set noshowmode
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set shiftround
set cursorline
set number

" iMproved
set nocompatible

" Color scheme options
syntax enable
set background=dark
colorscheme solarized

" Set leader
let mapleader=","

" Remap escape
imap jk <esc>

" Fast save
nmap <leader>w :w<cr>

" Fast save and quit
nmap <leader>x :x<cr>

" Fast quit
nmap <leader>q :q!<cr>

" Auto reload vimrc
augroup reload_vimrc " {
    autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END " }

" Autosave when leaving insertmode
"autocmd InsertLeave * if expand('%') != '' | update | endif

augroup CursorLine
  autocmd!
  autocmd VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  autocmd WinLeave * setlocal nocursorline
augroup END

" Removes trailing spaces
function! TrimWhiteSpace()
  %s/\s\+$//e
endfunction

nnoremap <silent> <Leader>rts :call TrimWhiteSpace()<CR>

" Removes trailing spaces on write
autocmd FileWritePre    * :call TrimWhiteSpace()
autocmd FileAppendPre   * :call TrimWhiteSpace()
autocmd FilterWritePre  * :call TrimWhiteSpace()
autocmd BufWritePre     * :call TrimWhiteSpace()

set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//

"---------------------------------------[Plugin Settings]----------------------------------------
"

"-----[Emmet]------
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall
"--------[Neocomplete]--------
" Disable AutoComplPop
"let g:acp_enableAtStartup = 0

" Use neocomplete
"let g:neocomplete#enable_at_startup = 1

" Set minimum syntax keyword length.
"let g:neocomplete#sources#syntax#min_keyword_length = 3

" <C-h>, <BS>: close popup and delete backword char.
 "inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
 "inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
 "inoremap <expr><C-y>  neocomplete#close_popup()
 "inoremap <expr><C-e>  neocomplete#cancel_popup()

 "inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
 "

" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1
"
" Enable heavy omni completion.
"if !exists('g:neocomplete#sources#omni#input_patterns')
    "let g:neocomplete#sources#omni#input_patterns = {}
  "endif

" <TAB>: completion.
"inoremap <expr><TAB>  pumvisible() ? "\<C-y>" : "\<TAB>"
"let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"

"--------[neosnippet]---------
" Plugin key-mappings.
"imap <C-k>     <Plug>(neosnippet_expand_or_jump)
"smap <C-k>     <Plug>(neosnippet_expand_or_jump)
"xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
"imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
"\ "\<Plug>(neosnippet_expand_or_jump)"
"\: pumvisible() ? "\<C-n>" : "\<TAB>"
"smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
"\ "\<Plug>(neosnippet_expand_or_jump)"
"\: "\<TAB>"

" For snippet_complete marker.
"if has('conceal')
  "set conceallevel=2 concealcursor=i
"endif

"--------[EasyMotion]--------
" Disable default mapping
let g:EasyMotion_do_mapping = 0
let g:EasyMotion_smartcase = 1

map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)
map  n <Plug>(easymotion-next)
map  N <Plug>(easymotion-prev)
nmap s <Plug>(easymotion-s)

map <Leader>l <Plug>(easymotion-lineforward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)

let g:EasyMotion_startofline = 0

"---------[tcomment]--------
vmap <Leader>c :TComment<CR>
nmap <Leader>c :TComment<CR>

"--------[Tmuxline]--------
let g:tmuxline_preset = {
      \'a'    : '#S',
      \'win'  : ['#I', '#W'],
      \'cwin' : ['#I', '#W', '#F'],
      \'y'    : ['%R', '%a', '%Y'],
      \'z'    : '#H'}

"--------[Airline]--------
let g:airline_powerline_fonts = 1
"let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#tmuxline#enabled = 1

"--------[RainbowParen]--------
au VimEnter * RainbowParentheses
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}'], ['"', '"']]
let g:rainbow#max_level = 16

"--------[tmux-navigator]--------
let g:tmux_navigator_save_on_switch = 1

"--------[Latex Live Preview]--------
"autocmd Filetype tex setl updatetime=5000
"let g:livepreview_previewer = 'open -a Skim'

"--------[Align]--------
vnoremap <silent> <Enter> :EasyAlign<cr>'"']]"

"--------[Syntastic]--------
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_cpp_compiler = 'g++'
let g:syntastic_cpp_compiler_options = ' -std=c++14 -stdlib=libc++'
