# Misc
export EDITOR=$(which vim)
export LANG="en_US.UTF-8"

# GCC
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# brew
export HOMEBREW_CASK_OPTS="--appdir=/Applications"

# mactex
eval `/usr/libexec/path_helper -s`

# path
export PATH="$HOME/.rbenv/shims:$HOME/bin:$HOME/.node/bin:$HOME/Library/Android/sdk/tools:$HOME/Library/Android sdk/platform-tools:/usr/local/sbin:$PATH"
export MANPATH="/usr/local/man:$MANPATH"

# python
export WORKON_HOME=~/.venvs
source /usr/local/bin/virtualenvwrapper.sh

# rbenv
#if which rbenv eval > /dev/null; then eval "$(rbenv init -)"; fi

#
export HOMEBREW_GITHUB_API_TOKEN=e5850abce7eb7348dd609a6ce738d621727eb76d
