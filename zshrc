######################################################################
#   antigen
#####################################################################
#
# Load Antigen
#export ZSH_TMUX_AUTOSTART=true

[ -e "${HOME}/.zsh_aliases" ] && source "${HOME}/.zsh_aliases"

source $HOME/.antigen/antigen.zsh

# Antigen Bundles
antigen bundle git
antigen bundle git-extras
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle command-not-found
antigen bundle ssh-agent
#antigen bundle common-aliases
antigen bundle history

# Python Plugins
antigen bundle python
antigen bundle pip
antigen bundle pep8
antigen bundle virtualenv
antigen bundle virtualenvwrapper

# Ruby Plugins
#antigen bundle ruby
#antigen bundle rake
#antigen bundle rbenv

# tmux
antigen bundle tmux
#antigen bundle tmuxinator


UNAME=`uname`
if [[ $UNAME == 'Darwin' ]]; then
	antigen bundle brew
	antigen bundle brew-cask
	antigen bundle gem
	antigen bundle osx
fi

antigen use oh-my-zsh
antigen theme agnoster

antigen apply
DEFAULT_USER='antoine'

#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir rbenv vcs virtualenv)
#POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(longstatus time)
